@extends('layout.layout')
@section('body')
    <div class="card border">
        <div class="card-body">
            @foreach($prods as $prod)

            @endforeach
            <form action="/api/produtos/{{$prods->id}}" method="post">
                @csrf
                <div class="form-group">

                    <label for="nome">Nome do Produto</label>
                    <input type="text" class="form-control" value="{{$prods->nome}}" name="nome" id="nome" placeholder="Produto">

                </div>

                <div class="form-group">

                    <label for="estoqueProduto">Tipo</label>
                    <input type="text" value="{{$prods->tipo}}" class="form-control" name="tipo" id="tipo" placeholder="Tipo do produto">

                </div>

                <div class="form-group">

                    <label for="categoria">Categoria do Produto</label>
                    <input type="text" value="{{$prods->categoria}}" class="form-control" name="categoria" id="categoria" placeholder="Categoria">

                </div>

                <div class="form-group">

                    <label for="tecnologia">Tecnologia</label>
                    <input type="text" value="{{$prods->tecnologia}}" class="form-control" name="tecnologia" id="tecnologia" placeholder="Tecnologia">

                </div>


                <div class="form-group">

                    <label for="classe">Classe</label>
                    <input type="text" value="{{$prods->classe}}" class="form-control" name="classe" id="classe" placeholder="Classe">

                </div>

                <div class="form-group">

                    <label for="unidade_medida">Medida</label>
                    <input type="text" value="{{$prods->unidade_medida}}" class="form-control" name="unidade_medida" id="unidade_medida" placeholder="Unidade de Medida">

                </div>

                <div class="form-group">

                    <label for="peso">Peso</label>
                    <input type="text" value="{{$prods->peso}}" class="form-control" name="peso" id="peso" placeholder="Peso">

                </div>


                <button class="btn btn-primary btn-sm">Salvar</button>
                <a href="/" class="btn btn-danger btn-sm">Cancelar</a>

            </form>
        </div>
    </div>
@endsection
