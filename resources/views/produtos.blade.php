@extends('layout.layout', ["current" => "produtos" ])

@section('body')

<div class="card border">
    <div class="card-body">
        <h5 class="card-title"><b>Lista de Resíduos 2020</b></h5>

        <table class="table table-ordered table-hover" id="tabelaProdutos">
            <thead>
                <tr>
                   <!-- <th>Código</th>-->
                    <th>Nome do Resíduo</th>
                    <th>Tipo de Resíduo</th>
                    <th>Categoria</th>
                   <!-- <th>Tecnologia de Tratamento</th>-->
                    <th>Classe</th>
                    <th>Unidade de Medida</th>
                    <th>Peso</th>
                    <th>Ações</th>

                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
       
    </div>
    <div class="card-footer">
        <button class="btn btn-sm btn-primary" role="button" onClick="novoProduto()">Importar Excel</a>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="dlgProdutos">
    <div class="modal-dialog" role="document"> 
        <div class="modal-content">
            <form class="form-horizontal " id="formProduto" method="POST" action="/api/processar" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title">Novos Produtos do Excel</h5>
                </div>
                <div class="modal-body">

					<div class="custom-file">
						<label class="custom-file-label">Produtos Recíclaveis</label>
						@csrf
						<input class="custom-file-input"  type="file" id="arquivo" name="arquivo" required="required">
						<input type="hidden" name="_token" value="{{@csrf_token()}}" class="hidden">
					</div>
			    </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Salvar</button>
                    <button type="cancel" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                </div>
            </form>
        </div>
    </div>
</div>


@endsection
     
     
     
@section('javascript')
<script type="text/javascript">
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        }
    });
    
    function novoProduto() {
        
        $('#dlgProdutos').modal('show');
    }
    
    function montarLinha(p) {
        var linha = "<tr>" +
            //"<td width='4%'>" + p.id + "</td>" +
            "<td width='20%'>" + p.nome + "</td>" +
            "<td>" + p.tipo + "</td>" +
            "<td width='15%'>" + p.categoria + "</td>" +
            //"<td width='15%'>" + p.tecnologia + "</td>" +
            "<td>" + p.classe + "</td>" +
            "<td>" + p.unidade_medida + "</td>" +
            "<td>" + p.peso + "</td>" +
                "<td width='14%' align='right'>" +
              '<a type="button" class="btn btn-sm btn-primary" href="api/editar/'+ p.id+'"> Editar </a> ' +
              '<a type="button" class="btn btn-sm btn-danger" href="api/deletar/'+ p.id+'"> Deletar </a>  ' +
            "</td>" +
            "</tr>";
        return linha;
    }
   
    function carregarProdutos() {
        $.getJSON('/api/produtos', function(produtos) {
            for(i=0;i<produtos.length;i++) {
                linha = montarLinha(produtos[i]);
                $('#tabelaProdutos>tbody').append(linha);
            }
        });        
    }


    $(function(){

        carregarProdutos();
    })
    
</script>
@endsection
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
