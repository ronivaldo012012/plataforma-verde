<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Support\Facades\Databases;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Produto;
class ProdutoController extends Controller
{

    public function import(){
        Produto::destroy('produtos');

        \Excel::load(Input::file('arquivo'),function($reader){
            $reader->each(function($sheet){
                foreach($sheet->toArray() as $row){

                    Produto::firstOrCreate($sheet->toArray());


                }
            });

        });

        return redirect('/api');
    }

    public function index()
    {
        $prod = Produto::all();
        return json_encode($prod);
        //return response()->json(['success' => 1], 200);
        //return view('produtos');
    }



   public function edit($id)
    {
        $prods = Produto::find($id);
        if(isset($prods)){
            return view('editar',compact('prods'));
        }
        //return redirect('/editar');
    }


    public function update(Request $request, $id)
    {
        $prod = Produto::find($id);
        if(isset($prod)){
            $prod->nome = $request->input('nome');
            $prod->tipo = $request->input('tipo');
            $prod->categoria = $request->input('categoria');
            $prod->tecnologia = $request->input('tecnologia');
            $prod->classe = $request->input('classe');
            $prod->unidade_medida = $request->input('unidade_medida');
            $prod->peso = $request->input('peso');
            $prod->save();


            return redirect('/api');
        }
    }


    public function destroy($id)
    {
        $prod = new Produto();
        $dados = $prod->find($id);
        if($dados){
            $dados->delete();
            return redirect('/api');
        }


    }
}
