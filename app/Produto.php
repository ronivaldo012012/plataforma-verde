<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Produto extends Model
{
    protected $table = 'produtos';
	public $timestamp = false;
    protected $fillable = array('nome','tipo','categoria','tecnologia',
        'classe','unidade_medida','peso');



}
