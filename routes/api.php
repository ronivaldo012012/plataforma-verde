<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::get('/', function () {
    return view('/produtos');
});

Route::get('/produtos','ProdutoController@index');

Route::post('/processar','ProdutoController@import');
Route::get('/editar/{id}','ProdutoController@edit');
Route::get('/deletar/{id}','ProdutoController@destroy');
Route::post('/produtos/{id}','ProdutoController@update');

/*
Route::prefix('v1')->group(function(){
    Route::name('produtos.')->group(function(){
        Route::resource('/','ProdutoController');
    });


});
*/